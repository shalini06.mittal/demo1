package com.basic.day3;

import java.time.LocalDate;

public class VariableScope {

	int v1; // instance variable
	//LocalDate joinDate = LocalDate.now();
	// instance methods
	public void m1(int v2)
	{
		v2 = 100;
		System.out.println(v1);
		System.out.println(v2);
	}
	
	public void m2()
	{
		int v2 = 10;
		System.out.println(v1);
		System.out.println(v2);
	}
	public static void main(String[] args) {
		
		VariableScope ob = new VariableScope();
		ob.v1 = 100;
		int v2 = 30;
		ob.m1(v2);
		
		VariableScope ob1 = new VariableScope();
		ob1.v1 = 500;
		v2 = 300;
		ob1.m1(v2);
		int x = 10;
		System.out.println(x);
		{
			int y = 20;
			System.out.println(y);
			System.out.println(x);
		}
		//System.out.println(y);

	}

}
