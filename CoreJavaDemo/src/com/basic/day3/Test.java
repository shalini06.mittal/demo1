package com.basic.day3;

public class Test {

	// instance variable
	String data = "Hello";
	
	/*
	 * 5 teams => Rogith -> team1
	 * Rogith => admin dept
	 */
	public void meet(String data) 
	// local variable and instance variable names are same, then local shadows
	// instance variable
	{
		System.out.println(data);
		// if local and instance variables names are same
		// this can be used to distinguish between the 2
		System.out.println(this.data);
	}
	
	public static void main(String[] args) {
		Test t1 = new Test();
		System.out.println(1);
		t1.meet("welcome");
		System.out.println(2);
		System.out.println(t1.data);
		Test t2 = new Test();
		System.out.println(3);
		t2.meet("bye");
		System.out.println(4);
		System.out.println(t2.data);
	}
}
