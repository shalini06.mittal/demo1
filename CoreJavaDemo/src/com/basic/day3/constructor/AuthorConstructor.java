package com.basic.day3.constructor;

/*
 * Create a class Cardboard with radius and color as instance variables
 * Create a default constructor and initialize default color to red and default radius  to 2
 * Create a parameterized constructor and initialize  color and radius as per user's input
 */
public class AuthorConstructor {

	private String aname;
	private String city;
	private String genre;
	private String email;
	private int age;
	
	/*
	 * Constructors
	 * 1) they are special methods used to initialize the instance members of a class
	 * 2) they have name same as the class name
	 * 3) they never return an value not even void
	 * 4) they are called automatically when the object is created
	 */
	public AuthorConstructor()
	{
		System.out.println("Author default constructor");
	}

	// constructor overloading
	public AuthorConstructor(String n,
			String c,
			String g,
			String e, int a)
	{
		System.out.println("Author parameterized constructor");
		aname = n.toUpperCase();
		age=a;
		city=c;
		email=e;
		genre=g;
	}
	public AuthorConstructor(String n,
			String c,
			String e, int a)
	{
		System.out.println("Author parameterized without genre  constructor");
		aname = n.toUpperCase();
		age=a;
		city=c;
		email=e;
	}
	
	
	
}
