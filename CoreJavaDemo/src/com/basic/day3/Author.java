package com.basic.day3;

public class Author {
	
	// access specifier
	// instance varables
	private String aname;
	String city;
	String genre;
	String email;
	int age;
	
	// local variables
	public void updateName(String name)
	{
		aname= name.toUpperCase();
	}
	
	// give some output
	public String getName()
	{
		return aname;
	}
	// methods
	// input values to initialize the variables wiht
	// method parameters 
	void initialize(String n,
	String c,
	String g,
	String e, int a)
	{
		//if(aname == null)
			
		aname = n.toUpperCase();
		age=a;
		city=c;
		email=e;
		genre=g;
	}
	
}
