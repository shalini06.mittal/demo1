package com.basic.day3;

import java.util.Scanner;

/**
 * Create a class Customer
 *
 */

public class TestAuthor {

	public static void main(String[] args) {
		//  x is a variable 
		int x ;
		// user-defined datatype
		// a1 of type Author
		Author a1;
		// allocate memory
		Scanner sc = new Scanner(System.in);
		a1 = new Author();
		a1.initialize("Roshni","Chennai","Thrill","roshni@gmail.com",22);
		a1.updateName("Rosshni");;
		System.out.println("Name "+a1.getName());
		
		Author a2;
		// allocate memory
		a2 = new Author();
		a2.initialize("Shalini","Mumbsi","Thrill","mumbai@gmail.com",22);
		
		System.out.println("Name "+a2.getName());
		
		
	}

}
