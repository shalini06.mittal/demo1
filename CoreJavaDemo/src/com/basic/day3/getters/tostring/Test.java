package com.basic.day3.getters.tostring;


public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AuthorGetterString a2 = new AuthorGetterString("Shalini","Mumbai","Adv","shalini@gmail.com",32);
		a2.setAname("Shalu");
		int x = 10;
		System.out.println(x);
		// string representation of a2 => use toString()
		System.out.println(a2.toString());
	}

}
