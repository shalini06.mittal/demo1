package com.basic.day3.getters.tostring;

/**
 * Create a class Address with foll members
 * country
 * city
 * pincode
 * 
 * default and parameterized constructor
 * getters and setters
 * 
 *
 */
public class AuthorGetterString{

	private String aname;
	private String city;
	private String genre;
	private String email;
	private int age;
	
	public AuthorGetterString()
	{
		System.out.println("Author default constructor");
	}

	
	public AuthorGetterString(String aname, String city, String genre, String email, int age) {
		super();
		this.aname = aname;
		this.city = city;
		this.genre = genre;
		this.email = email;
		this.age = age;
	}


	// getters and setters
	// setters => used to update => modify => mutators
	public void setAname(String aname)
	{
		this.aname = aname;
	}
	// getters => used to retreiver => accessors
	public String getAname()
	{
		return this.aname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	@Override
	public String toString() {
		return "aname=" + aname + ", city=" + city + ", genre=" + genre + ", email=" + email
				+ ", age=" + age;
	}
	
	
	
}
