package com.basic.day4;

public class Audio extends Item{

	private String track;

	public Audio(int id, String description, double price,String track) {
		super(id, description, price);
		this.track = track;
	}

	@Override
	public String toString() {
		return "Audio [track=" + track + super.toString()+"]";
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	@Override
	public void calculate() {
		// TODO Auto-generated method stub
		
	}

	
	
	
}
