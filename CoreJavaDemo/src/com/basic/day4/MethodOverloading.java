package com.basic.day4;

public class MethodOverloading {

	/**
	 * Method overloading is 
	 * 1) when the method name is same but any or all of the below are set
	 * a. number of parameters
	 * b. sequence of parameters
	 * c. type of parameters
	 * 
	 * 2) changing the return type or access speifier is NOT overloading
	 */
	public String encryption(String password, String algorithm)
	{
		return "12345";
	}
	public String encryption(String password)
	{
		return "00000";
	}
}
