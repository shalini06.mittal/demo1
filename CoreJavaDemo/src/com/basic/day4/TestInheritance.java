package com.basic.day4;

import com.basic.day3.Author;

public class TestInheritance {

	public static void main(String[] args) {
		//Author a1 = new Author();
		//System.out.println(a1 instanceof Item);
		//System.out.println(a1 instanceof Author);
		/**
		 * parent class reference item can be passed an object of the child class 
		 */
		Item item = null;//new Item();
		
		Book b1 = new Book(1, "thriller book", 1231, "Lets roll", "Jackelin");
		// upcasting
		item = b1;
		displayDetails(item);

		Audio audio = new Audio(2, "some music album", 120, "Beattles");
		item = audio;
		displayDetails(audio);
		
		Item b2 = new Book(4,"desc",34, "title","author");
	
	}
	public static void displayDetails(Item item)
	{
		// dynamic polymorphism
		/**
		 * reference decides which methods can be called => item
		 * object decides which class method will be called => book or audio
		 */
		System.out.println(item.getDescription());
		// downcasting
		if(item instanceof Book)
		{
			Book book = (Book)item;
			System.out.println(book.getTitle()); // tostring =>
		}
		else if(item instanceof Audio)
		{
			System.out.println(((Audio)item).getTrack());
		}
	}

}
