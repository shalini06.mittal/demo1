package com.basic.day4;
// parent or superclass or base
/**
 * 1) abstract keyword is used to craete an abstarct class
 * 2) abstract classes cannot be instantiated
 * 3) abstract classes can have method, instance variables and getters setters
 * 4) abstract class or parent class reference can be created and passed object of any of its child class
 * 5) abstract classes may or may not have abstract methods
 * 6) But IF ANY CLASS has even 1 abstract method , it is mandatory to declare that class abstract
 * 7) The child classes either implement the abstract methods or can themselves be declared as abstract
 *
 */
public abstract class Item {

	private int id;
	private String description;
	private double price;
	// abstract methods do not have any implementation
	public abstract void calculate();
	// implicit default constructor
	// PLEASE REMEMBER
	// IF YOU HAVE A PARAMETERIZED CONSTRUCTOR
	// THEN DO DEFIANE A DEFAULT CONSTRUCTOR
	// IT BECOMES EASY FOR INHERITANCE
	public Item() {
		//1
		
		//this(1,"",23);
		System.out.println("Item default constructor");
	}
	// this can be used to call the constructors of the same  or current class
	public Item(int id,  String description, double price) {
		//2
		//this();
		System.out.println("Item param constructor");
		this.id = id;
		
		this.description = description;
		this.price = price;
	}
	public Item(String description, double price) {
		//2
		//this();
		System.out.println("Item param constructor");

		
		this.description = description;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		System.out.println("to string of item class");
		return " id=" + id + ", description=" + description + ", price=" + price;
	}
	
	
}
