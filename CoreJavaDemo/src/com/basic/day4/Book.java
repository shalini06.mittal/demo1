package com.basic.day4;
// child or subclass or derived
public class Book extends Item{
	
	private String title;
	private String author;
	/*
	 * super can only be used to call immediate parent class constructor
	 * super has to be the 1st statement in the constructor
	 */
	public Book(int id, String description, double price, String title, String author) {
		
		super(id, description, price);
		//3
		System.out.println("Book param constructor");
		this.title = title;
		this.author = author;
		
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		System.out.println("to string of book class");
		return "Book [title=" + title + ", author=" + author + super.toString()+ "]";
	}

	@Override
	public void calculate() {
		// TODO Auto-generated method stub
		
	}
	
	

}
