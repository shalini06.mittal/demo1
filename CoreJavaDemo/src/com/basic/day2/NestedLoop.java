package com.basic.day2;

import java.util.Scanner;

public class NestedLoop {

	public static void main(String[] args) {

		/*
		 5
		 54
		 543
		 5432
		 54321
		 */
		for(int i=1;i<=3;i++)
		{
			for(int j=1;j<=i;j++)
			{
				System.out.print(j);
			}
			System.out.println();
		}
		System.out.println();
		for(int i=1;i<=3;i++)
		{
			for(int j=1;j<=i;j++)
			{
				System.out.print(i);
			}
			System.out.println();
		}
		
	}
}
