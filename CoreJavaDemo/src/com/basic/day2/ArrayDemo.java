package com.basic.day2;

public class ArrayDemo {

	public static void main(String[] args) {
		/*
		 * 1) container or variables to store more than 1 value of same type
		 * 2) special variables denoted with syntax []
		 * 3) arrays support indexing => 0 to length - 1
		 * 4) 
		 * 
		 */
		int x = 10; // 4 bytes
		// declaration
		int a[]; // 5 integer of values
		// creation of memory
		a = new int[5];
		
		// declaration and creation
		int arr[] = new int[5];
		
		// initialization or assign 
		arr[0] = 10;
		arr[1] = 5;
		arr[2] = 3;
		arr[3] = 15;
		arr[4] = 1;
		
		System.out.println(arr.length);
		System.out.println(arr[2]);
		
		// square of value at index 3 , ^ XOR
		int num = arr[3];
		System.out.println(num * num);
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i] * arr[i]);
			
		}
		/*
		 * WAP to take 5 nos as input and print the least value
		 */
	}

}
