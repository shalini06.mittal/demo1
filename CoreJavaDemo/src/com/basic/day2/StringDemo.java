package com.basic.day2;

import java.util.Scanner;

public class StringDemo {

	public static void main(String[] args) {
	
		/*
		 * take name as input from the user 
		 * and print following pattern without using nested loops
		 *  Input = INDIA
		 *  OUTPUT
		 * 
		 *  I
		 *  IN
		 *  IND
		 *  INDI
		 *  INDIA 
		 */
		
		Scanner sc = new Scanner(System.in);
		String s;
		s=sc.next();
		System.out.println(s);
	}

}
