package com.basic.day1;

import java.util.Scanner;

public class PrintOutput {

	public static void main(String[] args) {
		//They said, “ It’s going to rain today”
		char ch = 's';
		String s = "hello";
		System.out.println("They said, \"It’s going to rain today\"");
		System.out.println(" \" ");
		// L to R
		System.out.println("Result "+4+20);
		System.out.println(20+20+" Result");
		System.out.println('A'+'B' +"hello");// 65 + 66
		System.out.println('a'+"Result "+4+20);
		
		/**
		 * output of foll:
		 */
		int x = 10, y= 20;
		int z = x++ + x*y - --y;
		// z = 10 + 11 * 20 - 19 = 10 + 220 - 19 = 211
		System.out.println(z + " " + " " + x + " " + y);
		
		/**
		 * WAP to accept 5 nos as input from the user
		 * and print the sum of 5 nos without using loop
		 * and with only 2 variables
		 */
		
		Scanner sc = new Scanner(System.in);
		int sum = 0;
		int a = sc.nextInt();
		sum+=a;
		a = sc.nextInt();
		sum+=a;
		a = sc.nextInt();
		sum+=a;
		a = sc.nextInt();
		sum+=a;
		a = sc.nextInt();
		sum+=a;
		System.out.println(sum);
		/**
		 * ternary?
		 * voting application 
		 * take age as input and print if the person can vote or not?
		 * 
		 * if-else
		 * Suppose you are creating an application for an insurance company
		 * Criteria for providing insurance is as follows:
		 * 1. if married the person is insured
		 * 2. if unmarried and female with age >=25 she is insured
		 * 3. if unmarried and male with age >= 30 he is insured
		 * 4. for all other scenarios uninsured
		 */
		
	}

}
