package com.basic.day1;

import java.util.Scanner;

public class SwitchCaseDemo {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		System.out.println("1.Natural logarithmic \n 2.Absolute Value of the number \n 3.Square root of the number \n 4.Random Number between 0 and 1");
		int n = in.nextInt();
		switch(n) {
		 
		case 1:{
			System.out.println(Math.log(a));
			break;
		}
		case 2:{
			System.out.println(Math.abs(a));
			break;
		}
		case 3:{
			System.out.println(Math.sqrt(a));
			break;
			
		}
		case 4:{
			System.out.println(Math.random());
			break;
		}
		default:{
			System.out.println("invalid number");
		}
		}

	}

}
